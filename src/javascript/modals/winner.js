import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export function showWinnerModal(fighter) {
  const bodyElement = getWinnerDetails(fighter);
  const title = 'Winner';
  showModal({ title, bodyElement });  
}

function getWinnerDetails(fighter){
  const { name, source} = fighter;
  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'winner' });
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  nameElement.innerText = name;
  winnerDetails.append(nameElement);
  winnerDetails.append(imgElement);

  return winnerDetails;
}