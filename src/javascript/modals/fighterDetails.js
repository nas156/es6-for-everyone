import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({tagName: 'span', className: 'figter-health'});
  const attackElement = createElement({tagName: 'span', className: 'figter-attack'});
  const defenseElement = createElement({tagName: 'span', className: 'figter-defense'});
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = name;
  healthElement.innerText = '\nHealth: ' + health;
  attackElement.innerText = '\nAttack: ' + attack;
  defenseElement.innerText = '\nDefense: ' + defense + '\n';
  
  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(imgElement);

  return fighterDetails;
}
