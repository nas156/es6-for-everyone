export function fight(firstFighter, secondFighter) {
  var firstFighterHealth = firstFighter["health"];
  var secondFighterHealth = secondFighter["health"];
  
  while (true) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    if (secondFighterHealth <= 0) {
      return firstFighter;
    }
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
    if (firstFighterHealth <= 0) {
      return secondFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy);
  
}

export function getHitPower(fighter) {
  return fighter["attack"] * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter["defense"] * (Math.random() + 1);
}





